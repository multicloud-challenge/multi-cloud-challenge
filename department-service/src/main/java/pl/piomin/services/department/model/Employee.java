package pl.piomin.services.department.model;


import java.io.Serializable;

public class Employee implements Serializable {

	private String id;
	private String name;
	private int age;
	private String position;

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	private String departmentId;

	public Employee() {
	}

	public Employee(String name, int age, String position, String departmentId) {
		this.name = name;
		this.age = age;
		this.position = position;
		this.departmentId = departmentId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", organizationId="
				+ ", name=" + name + ", position=" + position + "]";
	}

}
