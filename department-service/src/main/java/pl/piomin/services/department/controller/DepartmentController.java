package pl.piomin.services.department.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pl.piomin.services.department.client.EmployeeClient;
import pl.piomin.services.department.model.Department;
import pl.piomin.services.department.model.Employee;
import pl.piomin.services.department.repository.DepartmentRepository;

@RestController
public class DepartmentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);
	
	@Autowired
	DepartmentRepository repository;
	@Autowired
	EmployeeClient employeeClient;
	
	@PostMapping("/")
	public Department add(@RequestBody Department dep) {
		LOGGER.info("Department add: {}", dep);
		Department department = new Department();
		department.setName(dep.getName());
		department.setOrganizationId(dep.getOrganizationId());
		Department savedDepartment = repository.save(department);
		/*if(!dep.getEmployees().isEmpty()) {
			List<Employee> employees = dep.getEmployees();
			for(Employee employee:employees) {
				employee.setDepartmentId(savedDepartment.getId());
				employeeClient.add(employee);
			}
		}*/
		savedDepartment.setEmployees(dep.getEmployees());
		return savedDepartment;
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<List<Department>> findAllByName(@PathVariable("name") String name) {
		LOGGER.info("Department find: name={}", name);
		List<Department> departments;
		if(null != repository.findAllByName(name)) {
			departments = new ArrayList<>(repository.findAllByName(name));
		}
		else {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(departments);
	}
	
	@GetMapping("/")
	public List<Department> findAll() {
		LOGGER.info("Department find");
		List<Department> departments = new ArrayList<>();
		for (Department department : repository.findAll()) {
			departments.add(department);
		}
		return departments;
	}
	
	@GetMapping("/organization/{organizationId}")
	public List<Department> findByOrganization(@PathVariable("organizationId") String organizationId) {
		LOGGER.info("Department find: organizationId={}", organizationId);
		List<Department> departments = new ArrayList<>();
		for (Department department : repository.findAll()) {
			departments.add(department);
		}
		return departments.stream().filter(d -> d.getOrganizationId().equals(organizationId)).collect(Collectors.toList());
	}
	
	@GetMapping("/organization/{organizationId}/with-employees")
	public List<Department> findByOrganizationWithEmployees(@PathVariable("organizationId") String organizationId) {
		LOGGER.info("Department find: organizationId={}", organizationId);
		List<Department> departments = new ArrayList<>();
		for (Department department : repository.findAll()) {
			departments.add(department);
		}
		List<Department> filteredDepartmentsByOrgID = departments.stream().filter(d -> d.getOrganizationId().equals(organizationId)).collect(Collectors.toList());
		filteredDepartmentsByOrgID.forEach(d -> d.setEmployees(employeeClient.findByDepartment(d.getId())));
		return filteredDepartmentsByOrgID;
	}
	
}
