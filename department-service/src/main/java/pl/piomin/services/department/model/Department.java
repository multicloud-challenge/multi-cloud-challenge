package pl.piomin.services.department.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "departments")
public class Department implements Serializable {

	@Id
	private String dep_id;
	private String name;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	private String organizationId;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	private List<Employee> employees;

	public Department() {
	}

	public Department(String name,String organizationId) {
		super();
		this.name = name;
		this.organizationId = organizationId;
	}

	public String getId() {
		return dep_id;
	}

	public void setId(String id) {
		this.dep_id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Department [id=" + dep_id + ", name=" + name + "]";
	}

}
