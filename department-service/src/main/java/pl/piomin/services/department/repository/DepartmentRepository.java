package pl.piomin.services.department.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.piomin.services.department.model.Department;

import java.util.List;

public interface DepartmentRepository extends CrudRepository<Department, String> {
    @Query(value = "{'name' : ?0}")
    Department findByName(String name);

    @Query(value = "{'name' : ?0}")
    List<Department> findAllByName(String name);
}
