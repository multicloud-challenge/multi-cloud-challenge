package pl.piomin.services.employee.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import pl.piomin.services.employee.model.Employee;
import pl.piomin.services.employee.repository.EmployeeRepository;

@RestController
public class EmployeeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
	
	@Autowired
	EmployeeRepository repository;
	
	@PostMapping
	public Employee add(@RequestBody Employee emp) {
		LOGGER.info("Employee add: {}", emp);
		Employee employee = new Employee();
		employee.setAge(emp.getAge());
		employee.setName(emp.getName());
		employee.setPosition(emp.getPosition());
		employee.setDepartmentId(emp.getDepartmentId());
		Employee savedEmployee = repository.save(employee);
		employee.setId(savedEmployee.getId());
		return employee;
	}
	
	@GetMapping("/{name}")
	public List<Employee> findAllByName(@PathVariable("name") String name) {
		LOGGER.info("Employee find: name={}", name);
		return repository.findAllByName(name);
	}
	
	@GetMapping("/")
	public List<Employee> findAll() {
		LOGGER.info("Employee find");
		List<Employee> employees = new ArrayList<>();
		for (Employee employee : repository.findAll()) {
			employees.add(employee);
		}
		return employees;
	}
	
	@GetMapping("/department/{departmentId}")
	public List<Employee> findByDepartment(@PathVariable("departmentId") String departmentId) {
		LOGGER.info("Employee find: departmentId={}", departmentId);
		List<Employee> employees = new ArrayList<>();
		for (Employee employee : repository.findAll()) {
			employees.add(employee);
		}
		return employees.stream().filter(e -> e.getDepartmentId().equals(departmentId)).collect(Collectors.toList());
	}
	
	/*@GetMapping("/organization/{organizationId}")
	public List<Employee> findByOrganization(@PathVariable("organizationId") Long organizationId) {
		LOGGER.info("Employee find: organizationId={}", organizationId);
		return repository.findByOrganization(organizationId);
	}*/
	
}
