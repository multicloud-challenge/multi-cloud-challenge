package pl.piomin.services.employee.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.piomin.services.employee.model.Employee;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, String> {
    @Query(value = "{'name' : ?0}")
    Employee findByName(String name);

    @Query(value = "{'name' : ?0}")
    List<Employee> findAllByName(String name);
}
