package pl.piomin.services.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.cloud.openfeign.EnableFeignClients;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import pl.piomin.services.employee.model.Employee;
import pl.piomin.services.employee.repository.EmployeeRepository;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
@OpenAPIDefinition(info =
	@Info(title = "Employee API", version = "1.0", description = "Documentation Employee API v1.0")
)
public class EmployeeApplication {

	@Autowired
	EmployeeRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeApplication.class, args);
	}

	@Bean
	public Docket swaggerPersonApi10() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
					.apis(RequestHandlerSelectors.basePackage("pl.piomin.services.employee.controller"))
					.paths(PathSelectors.any())
				.build()
				.apiInfo(new ApiInfoBuilder().version("1.0").title("Employee API").description("Documentation Employee API v1.0").build());
	}
	
	@Bean
	EmployeeRepository repository() {
		repository.deleteAll();
		repository.save(new Employee("John Smith", 34, "Analyst","1"));
		repository.save(new Employee("Darren Hamilton", 37, "Manager","1"));
		repository.save(new Employee( "Tom Scott", 26, "Developer","1"));
		repository.save(new Employee("Anna London", 39, "Analyst","1"));
		repository.save(new Employee("Patrick Dempsey", 27, "Developer","1"));
		repository.save(new Employee("Kevin Price", 38, "Developer","1"));
		repository.save(new Employee("Ian Scott", 34, "Developer","1"));
		repository.save(new Employee("Andrew Campton", 30, "Manager","1"));
		repository.save(new Employee("Steve Franklin", 25, "Developer","1"));
		repository.save(new Employee("Elisabeth Smith", 30, "Developer","1"));
		return repository;
	}
	
}
