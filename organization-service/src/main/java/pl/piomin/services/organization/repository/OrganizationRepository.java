package pl.piomin.services.organization.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.piomin.services.organization.model.Organization;

@Repository
public interface OrganizationRepository extends CrudRepository<Organization, String> {
    @Query(value = "{'name' : ?0}")
    Organization findByName(String name);
}
