package pl.piomin.services.organization.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import pl.piomin.services.organization.client.DepartmentClient;
import pl.piomin.services.organization.client.EmployeeClient;
import pl.piomin.services.organization.model.Department;
import pl.piomin.services.organization.model.Employee;
import pl.piomin.services.organization.model.Organization;
import pl.piomin.services.organization.repository.OrganizationRepository;

@RestController
public class OrganizationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationController.class);
	
	@Autowired
	OrganizationRepository repository;
	@Autowired
	DepartmentClient departmentClient;
	@Autowired
	EmployeeClient employeeClient;
	
	@PostMapping
	public Organization add(@RequestBody Organization org) {
		LOGGER.info("Organization add: {}", org);
		Organization organization = new Organization();
		organization.setName(org.getName());
		organization.setAddress(org.getAddress());
		Organization savedOrganization = repository.save(organization);
		if(!org.getDepartments().isEmpty()) {
			List<Department> departments = org.getDepartments();
			for(Department department:departments) {
				department.setOrganizationId(savedOrganization.getId());
				Department savedDepartment = departmentClient.add(department);
				department.setId(savedDepartment.getId());
			}
		}
		savedOrganization.setDepartments(org.getDepartments());
		for(Department department: savedOrganization.getDepartments()) {
			if(!department.getEmployees().isEmpty()) {
				List<Employee> employees = department.getEmployees();
				for(Employee employee:employees) {
					employee.setDepartmentId(department.getId());
					Employee savedEmployee = employeeClient.add(employee);
					employee.setId(savedEmployee.getId());
				}
			}
		}
		return savedOrganization;
	}
	
	@GetMapping
	public List<Organization> findAll() {
		LOGGER.info("Organization find");
		List<Organization> organizations = new ArrayList<>();
		for (Organization organization : repository.findAll()) {
			organizations.add(organization);
		}
		return organizations;
	}
	
	@GetMapping("/{name}")
	public Organization findByName(@PathVariable("name") String name) {
		LOGGER.info("Organization find: name={}", name);
		return repository.findByName(name);
	}

	@GetMapping("/{name}/with-departments")
	public Organization findByIdWithDepartments(@PathVariable("name") String name) {
		LOGGER.info("Organization find: name={}", name);
		Organization organization = repository.findByName(name);
		organization.setDepartments(departmentClient.findByOrganization(organization.getId()));
		return organization;
	}
	
	@GetMapping("/{name}/with-departments-and-employees")
	public Organization findByIdWithDepartmentsAndEmployees(@PathVariable("name") String name) {
		LOGGER.info("Organization find: name={}", name);
		Organization organization = repository.findByName(name);
		organization.setDepartments(departmentClient.findByOrganizationWithEmployees(organization.getId()));
		return organization;
	}
	
/*	@GetMapping("/{id}/with-employees")
	public Organization findByIdWithEmployees(@PathVariable("id") Long id) {
		LOGGER.info("Organization find: id={}", id);
		Organization organization = repository.findById(id).get();
		organization.setEmployees(employeeClient.findByOrganization(organization.getId()));
		return organization;
	}*/
	
}
