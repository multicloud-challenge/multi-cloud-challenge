package pl.piomin.services.organization.model;


import java.io.Serializable;
import java.util.List;

public class Department implements Serializable {

	private String id;
	private String name;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	private String organizationId;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	private List<Employee> employees;

	public Department() {
	}

	public Department(String name, String organizationId) {
		super();
		this.name = name;
		this.organizationId = organizationId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "]";
	}

}
