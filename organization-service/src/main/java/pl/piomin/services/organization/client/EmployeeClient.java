package pl.piomin.services.organization.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.piomin.services.organization.model.Employee;

import java.util.List;

@FeignClient(name = "employee-service")
public interface EmployeeClient {

    @PostMapping
    Employee add(@RequestBody Employee emp);

}
