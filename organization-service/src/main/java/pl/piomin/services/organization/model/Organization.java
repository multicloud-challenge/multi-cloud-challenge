package pl.piomin.services.organization.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import javax.annotation.Generated;
import java.io.Serializable;
import java.util.List;


@Document(collection = "organizations")
public class Organization implements Serializable {

	@Id
	private String org_id;

	private String name;
	private String address;

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	private List<Department> departments;

	public Organization() {

	}
	
	public Organization(String name, String address) {
		this.name = name;
		this.address = address;
	}
	public String getId() {
		return org_id;
	}

	public void setId(String id) {
		this.org_id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Organization [id=" + org_id + ", name=" + name + ", address=" + address + "]";
	}

}
